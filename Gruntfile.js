module.exports = function( grunt ) {

	require('es6-promise').polyfill();

	var postcss     = require('postcss');
	var browserSync = require('browser-sync');

	'use strict';

	// Project configuration
	grunt.initConfig( {

		pkg: grunt.file.readJSON( 'package.json' ),

		// Compile SASS
		sass: {

			dev: {
				options: {
					style: 'expanded',
					cacheLocation: '<%= pkg.themesDir %>/eco-theme/assets/css/scss/.sass-cache/',
					sourceMap: true
				},
				files: {
					'<%= pkg.themesDir %>/eco-theme/assets/css/eco.css' : '<%= pkg.themesDir %>/eco-theme/assets/css/scss/main.scss'
				}
			},

			prod: {
				options: {
					noCache: true
				},
				files: {
					'<%= pkg.themesDir %>/eco-theme/assets/css/eco.min.css' : '<%= pkg.themesDir %>/eco-theme/assets/css/scss/main.scss'
				}
			},

			rare: {
				options: {
					noCache: true
				},
				files: {
					'<%= pkg.themesDir %>/eco-theme/assets/css/kitchen-sink.css' : '<%= pkg.themesDir %>/eco-theme/assets/css/scss/kitchen-sink.scss'
				}
			}
		},

		// Concatenate all js files
		concat: {
			options: {
				separator: ';',
				preserveComments: true,
				sourceMap: function( dest ) { return dest + '.map' },
				sourceMappingURL: function( dest ) { return dest.replace(/^.*[\\\/]/, '') + '.map' },
				sourceMapRoot: '/',
				beautify: true
			},

			dist: {
				src: [

				// Vendor scripts (add files individually - located in js/externals directory)
				'<%= pkg.themesDir %>/eco-theme/assets/js/externals/**/*.js',

				// Include custom scripts
				'<%= pkg.themesDir %>/eco-theme/assets/js/src/**/*.js',
				],

				// Finally, concatenate all the files above into one single file
				dest: '<%= pkg.themesDir %>/eco-theme/assets/js/eco.js',
			},
		},

		// JS Minification & Concatenation
		uglify: {

			prod: {
				options: {
					preserveComments: false,
					banner: '/* <%= pkg.homepage %> * Copyright (c) <%= grunt.template.today("yyyy") %> */\n',
					mangle: { except: ['jQuery'] }
				},
				files: {
					'<%= pkg.themesDir %>/eco-theme/assets/js/eco.min.js': ['<%= pkg.themesDir %>/eco-theme/assets/js/src/main.js']
				}
			}

		},

	postcss: {

		options: {
			processors: [
				require('pixrem')(), // add fallbacks for rem units
				require('autoprefixer')({browsers: 'last 2 versions'}), // add vendor prefixes
				require('cssnano')() // minify the result
			]
		},

		dist: {
			src: '<%= pkg.themesDir %>/eco-theme/assets/css/eco.min.css'
		},

		rare: {
			src: '<%= pkg.themesDir %>/eco-theme/assets/css/kitchen-sink.css'
		}
	},

		// Watch for changes
		watch: {

			sass_dev: {
				files: [
					'<%= pkg.themesDir %>/eco-theme/assets/css/*/**/*.scss',
					'!<%= pkg.themesDir %>/eco-theme/assets/css/scss/kitchen-sink.scss'
				],
				tasks: ['sass:dev', 'bs-inject-css'],
				options: {
					spawn: false
				}
			},

			sass_rare: {
				files: ['<%= pkg.themesDir %>/eco-theme/assets/css/scss/kitchen-sink.scss'],
				tasks: ['sass:rare', 'postcss:rare', 'bs-inject-css'],
				options: {
					spawn: false
				}
			},

			scripts: {
				files: ['<%= pkg.themesDir %>/eco-theme/assets/js/*/**/*.js'],
				tasks: ['concat', 'bs-reload'],
				options: {
					debounceDelay: 500,
					spawn: false
				}
			},

			all: {
				files: [
					'<%= pkg.themesDir %>/*/**/*.php'
				],
				tasks: ['bs-reload'],
				options: {
					spawn: false
				}
			}
		}
	} );

	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-postcss');

	// Init BrowserSync manually
	grunt.registerTask('bs-init', function () {
		var done = this.async();
		browserSync({
			open: 'ui',
			timestamps: true,
			proxy: {
				target: 'eco.me'
			}
		}, function (err, bs) {
			done();
		});
	});

	// Inject CSS files to the browser
	grunt.registerTask('bs-inject-css', function () {
		browserSync.reload([
			'<%= pkg.themesDir %>/eco-theme/assets/css/eco.css',
			'<%= pkg.themesDir %>/eco-theme/assets/css/kitchen-sink.css'
		]);
	});

	// Reload browser
	grunt.registerTask('bs-reload', function () {
		browserSync.reload([
			'<%= pkg.themesDir %>/eco-theme/assets/js/eco.js',
			'<%= pkg.themesDir %>/**/*.php'
		]);
	});

	// Default task.
	grunt.registerTask( 'default', ['concat', 'sass:dev', 'bs-init', 'watch'] );
	grunt.registerTask( 'build', ['concat', 'uglify', 'sass', 'postcss'] );

	grunt.util.linefeed = '\n';

};