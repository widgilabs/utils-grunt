[TOC]

# Intro

Usamos o GruntJS nos nossos projetos. Pretende-se que aqui seja um sítio onde está documentado que plugins do Grunt usamos e como se configuram e também um template base para usarmos nos projetos.

O Grunt serve essencialmente para automatizar tarefas e corre a partir do terminal.

No caso de projetos WordPress usamos o Grunt para minificar e fazer o uglify do Javascript, minificar e compilar o CSS a partir de SASS, configurar o browsersync, etc.

# Instalação

O Grunt precisa do [Node.JS e NPM](http://nodejs.org/download/). 

Globalmente no sistema o que convém instalar é o grunt-cli e não o grunt. Isto porque o grunt-cli permite que tenhamos versões diferentes do Grunt para vários projetos.

Para instalar fazer:

```
#!python
npm install -g grunt-cli
```

# Começar a usar

Para o grunt funcionar vamos precisar de criar dois ficheiros na raiz do nosso tema WordPress: package.json e um Gruntfile. No package.json é especificado alguns dados sobre os módulos que vamos usar.
O Gruntfile é usado para configurar as tasks e carregar os Grunt plugins.
Tanto o package.json como o Gruntfile é para serem adicionados ao Git.

Exemplo de um package.json

```
#!Javascript

{
  "name": "project_name",
  "version": "1.0.0",
  "title": "Project Title",
  "homepage": "http://www.yoursite.com"
}
```

Exemplo de um Gruntfile.js

```
#!Javascript
module.exports = function( grunt ) {
 
  grunt.initConfig({
    // Tasks que o Grunt deve executar
  });
};
```

Esse comando instala o grunt localmente para o projeto e grava uma referência em devDependencies, no package.json.

```
#!javascript
npm install grunt --save-dev
```

Após este comando é criada uma directoria na raiz chamada node_modules. Aqui ficam os ficheiros dos módulos que forem instalados usando o npm (incluindo os plugins do Grunt).
É importante usar o –save-dev para que o package.json esteja sempre actualizado no Git e quem quiser começar a trabalhar no projeto possa instalar os módulos necessários fazendo apenas npm install.

# Tarefas/Plugins Comuns que usamos

## Minificação

Instalar o plugin:

```
#!javascript
npm install grunt-contrib-uglify --save-dev
```

Configurar:

```
#!python
grunt.loadNpmTasks('grunt-contrib-uglify'); 
```

## Compilação de SASS/grunt-sass

Instalar o plugin:

```
#!python
npm install grunt-sass --save-dev
```

Para configurar no Gruntfile.js

```
#!python
grunt.loadNpmTasks('grunt-sass');
```

## Detectar mudanças nos ficheiros/watch

Instalar o plugin:

```
#!python
npm install grunt-contrib-watch --save-dev
```

Configurar

```
#!python
grunt.loadNpmTasks('grunt-contrib-concat');
```

## Aplicar vários post-processors/postcss

O postcss sozinho não faz quase nada, acenta também em plugins que no fundo são os *processors* para o postcss.

Instalar o plugin:

```
#!python
npm install grunt-postcss --save-dev
```

Configurar no Gruntfile.js

```
#!python
grunt.loadNpmTasks('grunt-postcss');
```

Um dos plugins do postcss que usamos é o autoprefixer:

Para instalar:

```
#!python
npm install autoprefixer --save-dev
```

Para configurar:

```
#!python
processors: [
  require('autoprefixer')()
]
```

## Atualização do browser/live reload-browsersync

Instalar o plugin:

```
#!python
npm install browser-sync --save-dev
```

Configurar no Gruntfile.js

```
#!python
    // Init BrowserSync manually
    grunt.registerTask('bs-init', function () {
        var done = this.async();
        browserSync({
            open: 'ui',
            timestamps: true,
            proxy: {
                target: 'eco.me'
            }
        }, function (err, bs) {
            done();
        });
    });
 
    // Inject CSS files to the browser
    grunt.registerTask('bs-inject-css', function () {
        browserSync.reload([
            '<%= pkg.themesDir %>/eco-theme/assets/css/eco.css',
            '<%= pkg.themesDir %>/eco-theme/assets/css/kitchen-sink.css'
        ]);
    });
 
    // Reload browser
    grunt.registerTask('bs-reload', function () {
        browserSync.reload([
            '<%= pkg.themesDir %>/eco-theme/assets/js/eco.js',
            '<%= pkg.themesDir %>/**/*.php'
        ]);
    });
```

# Template

Ver os ficheiros que estão neste repo para um exemplo de uma configuração final do Grunt para projetos WordPress.

# Resources

* http://24ways.org/2013/grunt-is-not-weird-and-hard/
* http://gruntjs.com/plugins
* http://markdalgleish.github.io/presentation-build-wars-gulp-vs-grunt/